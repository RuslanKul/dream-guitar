import React from 'react';
import styles from './Search.module.scss';
import magnifier from '../../asets/icon/search_mdfonllkn502.svg';
import close from '../../asets/icon/cross_imkr39zvux4o.svg';

function Search() {
  return <div className={styles.search}></div>;
}

export default Search;
