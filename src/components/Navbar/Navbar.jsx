import React from 'react';
import './Navbar.scss';
import { Link } from 'react-router-dom';

const Navbar = (props) => {
  const { name, sublinks, path } = props;
  const [open, setOpen] = React.useState(false);

  return (
    <>
      <div onMouseEnter={() => setOpen(true)} onMouseLeave={() => setOpen(false)}>
        <Link to={path} className="linkNavTitle">
          <h2 className="navTitle">{name}</h2>
        </Link>
        {open && (
          <div className="subMenu">
            <div className="subNavMenu">
              {sublinks?.map((list) => (
                <Link to={list.link}>
                  <li key={list.id} link={list.link} className="navLInk">
                    {list.name}
                  </li>
                </Link>
              ))}
            </div>
          </div>
        )}
      </div>
    </>
  );
};

export default Navbar;
