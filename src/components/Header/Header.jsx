import basket from '../../asets/icon/basket.png';
import man from '../../asets/icon/men.png';
import { links } from '../../asets/links';
import { Link } from 'react-router-dom';
import './Header.scss';
import Navbar from '../Navbar/Navbar';

function Header() {
  return (
    <div className="header">
      <div className="container">
        <Link to="/">
          <div className="logo">dream guitar</div>
        </Link>
        <ul className="nav">
          {links.map((nav) => (
            <Navbar key={nav.id} path={nav.path} name={nav.name} sublinks={nav.sublinks} />
          ))}
        </ul>
        <div className="basket">
          <Link to="basket">
            <img src={basket} alt="" />
          </Link>
          <img src={man} alt="" />
        </div>
      </div>
    </div>
  );
}

export default Header;
