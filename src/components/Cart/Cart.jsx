import './Cart.scss';

function Cart({ title, price, image }) {
  return (
    <>
      <div className="cart">
        <img src={process.env.PUBLIC_URL + '/guitars/' + image} alt="" />
        <div className="titleCart">
          <p>{title}</p>
          <button>+</button>
        </div>
        <div className="price">
          <p>Цена:</p>
          <p>{price}руб.</p>
        </div>
      </div>
    </>
  );
}
export default Cart;
