export const links = [
  {
    id: 1,
    name: 'electric',
    path: 'type/1',
    sublinks: [
      { id: 1, name: 'b.c.rich', link: `type/${1}/brand/${1}` },
      { id: 2, name: 'gibson', link: `type/${1}/brand/${2}` },
      { id: 3, name: 'dean', link: `type/${1}/brand/${3}` },
      { id: 4, name: 'epiphone', link: `type/${1}/brand/${4}` },
      { id: 5, name: 'esp', link: `type/${1}/brand/${5}` },
      { id: 6, name: 'fender', link: `type/${1}/brand/${6}` },
      { id: 7, name: 'ibanez', link: `type/${1}/brand/${7}` },
      { id: 8, name: 'jackson', link: `type/${1}/brand/${8}` },
      { id: 9, name: 'kramer', link: `type/${1}/brand/${9}` },
      { id: 10, name: 'ltd', link: `type/${1}/brand/${10}` },
      { id: 11, name: 'schecter', link: `type/${1}/brand/${11}` },
      { id: 12, name: 'yamaha', link: `type/${1}/brand/${12}` },
    ],
  },
  { id: 2, name: 'acoustic', path: 'type/2' },
  { id: 3, name: 'bass', path: 'type/3' },
  // { id: '4', name: 'guitar accessories', path: 'accessories' },
];
