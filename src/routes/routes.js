import { createBrowserRouter } from 'react-router-dom';
import Home from '../pages/Home';
import Basket from '../pages/Basket';
import { GitarTypePage } from '../pages/GitarTypePage';
import { GitarBrandPage } from '../pages/GitarBrandPage';
import { MainLayout } from '../pages/MainLayout';

export const routes = createBrowserRouter([
  {
    path: '',
    element: <MainLayout />,
    children: [
      {
        path: '',
        element: <Home />,
      },
      {
        path: 'basket',
        element: <Basket />,
      },
      {
        path: 'type/:typeId',
        element: <GitarTypePage />,
        children: [
          {
            path: 'brand/:brandId',
            element: <GitarBrandPage />,
          },
        ],
      },
    ],
  },
]);
