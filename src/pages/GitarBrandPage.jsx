import React from 'react';
import { useParams } from 'react-router-dom';
import Brands from '../asets/guitarBrands.json';
import allGuitars from '../asets/guitar.json';
import Cart from '../components/Cart/Cart';

export const GitarBrandPage = () => {
  const params = useParams();

  const getCurrentBrand = () => {
    const result = Brands.find(({ id }) => {
      return id === Number(params.brandId);
    });

    return result?.title ?? 'Not Found';
  };

  const getCurrentGuitars = () => {
    return allGuitars.filter((guitar) => guitar.brand_id === Number(params.brandId));
  };
  return (
    <div>
      <h2>{getCurrentBrand()}</h2>
      <div className="wrapper">
        {getCurrentGuitars().map((obj) => (
          <Cart key={obj.guitar_id} {...obj} />
        ))}
      </div>
    </div>
  );
};
