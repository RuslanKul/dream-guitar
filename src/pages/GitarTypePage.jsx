import React from 'react';
import { Route, Routes, useParams } from 'react-router-dom';
import { GitarBrandPage } from './GitarBrandPage';
import GuitarTypes from '../asets/guitarTypes.json';

export const GitarTypePage = () => {
  const params = useParams();

  const getCurrentType = () => {
    const result = GuitarTypes.find(({ id }) => {
      return id === Number(params.typeId);
    });

    return result?.title ?? 'Not Found';
  };

  return (
    <div>
      {getCurrentType()}
      <Routes>
        <Route path="">
          <Route exact path="brand/:brandId" element={<GitarBrandPage />} />
        </Route>
      </Routes>
    </div>
  );
};
