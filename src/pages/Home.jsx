import React from 'react';
import Search from '../components/Search';

const Home = () => {
  return (
    <div className="content">
      <div className="title">
        <h1>Gibson</h1>
        <Search />
      </div>
      <div className="wrapper"></div>
    </div>
  );
};
export default Home;
